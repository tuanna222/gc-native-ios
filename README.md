Native iOS Engine for Game Closure SDK with many bug fixes and new features.
Please show commits list on master branch for more informations.
Fork from https://github.com/gameclosure/native-ios


# Game Closure iOS Engine

This is the native `Tealeaf` platform for iOS devices.

See the [native documentation](http://docs.gameclosure.com/#native).

## License

The Game Closure iOS engine is released under the GPLv3.  See LICENSE.txt
